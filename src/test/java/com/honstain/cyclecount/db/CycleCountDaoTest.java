package com.honstain.cyclecount.db;

import com.honstain.cyclecount.CycleCountServiceApplication;
import com.honstain.cyclecount.CycleCountServiceConfiguration;
import com.honstain.cyclecount.api.CycleCount;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit5.DropwizardAppExtension;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(DropwizardExtensionsSupport.class)
class CycleCountDaoTest {
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config.yml");

    static final DropwizardAppExtension<CycleCountServiceConfiguration> DROPWIZARD =
            new DropwizardAppExtension<>(CycleCountServiceApplication.class, CONFIG_PATH);

    private static CycleCountDao cycleCountDao;

    @BeforeAll
    static void setUpAll() {
        Jdbi jdbi = new JdbiFactory().build(
                DROPWIZARD.getEnvironment(),
                DROPWIZARD.getConfiguration().getDataSourceFactory(),
                "postgresql-test"
        );
        cycleCountDao = jdbi.onDemand(CycleCountDao.class);
        cycleCountDao.createCycleCountTable();
    }

    @BeforeEach
    void setup() {
        cycleCountDao.dropCycleCountTable();
    }

    @Test
    void testSelectAllEmpty() {
        assertEquals(List.of(), cycleCountDao.selectAll());
    }

    @Test
    void testInsertAndSelectAll() {
        final CycleCount count = new CycleCount(UUID.randomUUID(), "LOC-01", "SKU-01", 4);

        cycleCountDao.insert(
                count.getId(),
                count.getLocation(),
                count.getSku(),
                count.getQty()
        );
        assertEquals(List.of(count), cycleCountDao.selectAll());
    }
}
