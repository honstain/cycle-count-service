package com.honstain.cyclecount;

import com.honstain.cyclecount.api.CycleCount;
import com.honstain.cyclecount.db.CycleCountDao;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit5.DropwizardAppExtension;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(DropwizardExtensionsSupport.class)
class IntegrationTest {
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config.yml");

    static final DropwizardAppExtension<CycleCountServiceConfiguration> DROPWIZARD =
            new DropwizardAppExtension<>(CycleCountServiceApplication.class, CONFIG_PATH);

    private static CycleCountDao cycleCountDao;

    @BeforeAll
    static void setUpAll() {
        Jdbi jdbi = new JdbiFactory().build(
                DROPWIZARD.getEnvironment(),
                DROPWIZARD.getConfiguration().getDataSourceFactory(),
                "postgresql-test"
        );
        cycleCountDao = jdbi.onDemand(CycleCountDao.class);
        cycleCountDao.createCycleCountTable();
    }

    @BeforeEach
    void setup() {
        cycleCountDao.dropCycleCountTable();
    }

    @Test
    void testGetCycleCounts() {
        final CycleCount newCycleCount = new CycleCount(UUID.randomUUID(), "LOC-01", "SKU-01", 4);

        cycleCountDao.insert(newCycleCount.getId(), newCycleCount.getLocation(), newCycleCount.getSku(), newCycleCount.getQty());

        final List<CycleCount> expected = List.of(newCycleCount);
        final List<CycleCount> cycleCounts = DROPWIZARD.client()
                .target("http://localhost:" + DROPWIZARD.getLocalPort() + "/")
                .request()
                .get(new GenericType<>() {});
        assertEquals(expected, cycleCounts);
    }

    @Test
    void testCreateCycleCounts() {
        final CycleCount newCycleCount = new CycleCount(UUID.randomUUID(), "LOC-01", "SKU-01", 4);

        final Response response = DROPWIZARD.client()
                .target("http://localhost:" + DROPWIZARD.getLocalPort() + "/")
                .request()
                .post(Entity.json(newCycleCount));
        assertEquals(204, response.getStatus());
    }

    @Test
    void testCreateCycleCountsAndGetCycleCounts() {
        final CycleCount newCycleCount = new CycleCount(UUID.randomUUID(), "LOC-01", "SKU-01", 4);

        final Response response = DROPWIZARD.client()
                .target("http://localhost:" + DROPWIZARD.getLocalPort() + "/")
                .request()
                .post(Entity.json(newCycleCount));
        assertEquals(204, response.getStatus());

        final List<CycleCount> cycleCounts = DROPWIZARD.client()
                .target("http://localhost:" + DROPWIZARD.getLocalPort() + "/")
                .request()
                .get(new GenericType<>() {});

        assertEquals(List.of(newCycleCount), cycleCounts);
    }
}


