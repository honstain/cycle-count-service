package com.honstain.cyclecount.db;

import com.honstain.cyclecount.api.CycleCount;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;
import java.util.UUID;

public interface CycleCountDao {

    @SqlUpdate(
            "CREATE TABLE IF NOT EXISTS cycle_count (\n" +
            "    id UUID PRIMARY KEY,\n" +
            "    location text,\n" +
            "    sku text,\n" +
            "    qty int,\n" +
            "    created_at timestamp default current_timestamp\n" +
            ");"
    )
    void createCycleCountTable();

    @SqlUpdate("TRUNCATE cycle_count")
    void dropCycleCountTable();

    @SqlUpdate(
            "INSERT INTO cycle_count (id, location, sku, qty) " +
            "VALUES (:id, :location, :sku, :qty)"
    )
    void insert(
            @Bind("id") UUID id,
            @Bind("location") String location,
            @Bind("sku") String sku,
            @Bind("qty") Integer qty
    );

    @SqlQuery("SELECT id, location, sku, qty FROM cycle_count")
    @RegisterRowMapper(CycleCountMapper.class)
    List<CycleCount> selectAll();
}
