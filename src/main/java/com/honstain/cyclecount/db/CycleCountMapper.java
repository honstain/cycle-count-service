package com.honstain.cyclecount.db;

import com.honstain.cyclecount.api.CycleCount;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class CycleCountMapper implements RowMapper<CycleCount> {
    @Override
    public CycleCount map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new CycleCount(
                UUID.fromString(rs.getString("id")),
                rs.getString("location"),
                rs.getString("sku"),
                rs.getInt("qty")
        );
    }
}
