package com.honstain.cyclecount.resources;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.honstain.cyclecount.api.CycleCount;
import com.honstain.cyclecount.db.CycleCountDao;
import com.rabbitmq.client.Channel;
import org.jdbi.v3.core.Jdbi;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class CycleCountResource {

    public final Jdbi jdbi;
    public final CycleCountDao dao;
    public final Channel channel;

    public CycleCountResource(Jdbi jdbi, Channel channel) {
        this.jdbi = jdbi;
        this.dao = jdbi.onDemand(CycleCountDao.class);
        this.channel = channel;
    }

    @GET
    @Timed
    public List<CycleCount> getCycleCounts() {
        List<CycleCount> result = this.dao.selectAll();
        System.out.println(result);
        return result;
    }

    @POST
    @Timed
    public void createCycleCount(CycleCount cycleCount) throws IOException {
        this.dao.insert(cycleCount.getId(), cycleCount.getLocation(), cycleCount.getSku(), cycleCount.getQty());

        ObjectMapper mapper = new ObjectMapper();
        System.out.println(mapper.writeValueAsString(cycleCount));
        final String exchangeName = "cycle-count-create";
        //byte[] messageBodyBytes = "{\"id\":\"A1001\"}".getBytes();
        byte[] messageBodyBytes = mapper.writeValueAsString(cycleCount).getBytes();

        this.channel.basicPublish(exchangeName, "*", null, messageBodyBytes);
    }
}
